import EStyleSheet from 'react-native-extended-stylesheet';
import { isIphoneX } from '../../../libs/Utils';
import { Fonts } from '../../../theme';
import { Platform } from 'react-native';
export default EStyleSheet.create({
  mainView: {
    height: '60%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  innerImage: {
    height: '320rem',
    width: '330rem',
    resizeMode: 'contain'
  },
  innerTextView: {},

  innerText: {
    fontSize: '25rem',
    ...Fonts.style.robootBoldWhite,
    textAlign: 'center',
    paddingVertical: isIphoneX() ? '25rem' : '13rem'
  },
  innerDescriptionText: {
    fontSize: '14rem',
    ...Fonts.style.mediumBluegrey,
    textAlign: 'center',
    paddingHorizontal: '50rem'
  },
  getStartBtn: {
    height: '50rem',
    width: '330rem',
    borderRadius: '8rem',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '20rem'
  },
  shadowStyle: {
    marginTop: Platform.OS === 'ios' ? '70rem' : '73rem',
    marginBottom: '15rem'
  },
  bottomText: {
    ...Fonts.style.mediumLightBlueGrey,
    textAlign: 'center',
    fontSize: '15rem'
  },
  buttonView: {
    bottom: isIphoneX() ? '20rem' : '18rem',
    position: 'relative'
  },
  top: {
    top: Platform.OS === 'ios' ? 0 : '20rem'
  },
  ScanQrNoSession:{
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color : '#ACB5C5',
    margin: 20
  },
  ScanQr: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 0,
    color : 'white'
  }
  ,
  Qrcode : {
    width: 335, 
    height: 304 ,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom:'200rem',
    marginTop:100,
    borderRadius: '10rem',
    borderColor:'#3E434E',
    borderWidth:2,
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 5,
},
shadowOpacity: 0.34,
shadowRadius: 6.27,
elevation: 10,
  }
});
