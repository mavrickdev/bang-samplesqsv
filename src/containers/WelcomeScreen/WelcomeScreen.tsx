import React from 'react';
import { View, Image, Text, BackHandler,Linking } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container, SeparatorLine, Button, BangHeader,ButtonEmpty } from '../../components';
import { Images } from '../../theme';
import darkStyle from './Styles/WelcomeScreen';
import lightStyle from './Styles/WelcomeLightScreen';
import { SelectedTheme } from './../../actions/userAction';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
export interface Props {
  navigation: any;
  addSelectedTheme: any;
  themeType: any;
}

class WelcomeScreen extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }
  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  };
  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  };
  backHandler = () => {
    const { navigation } = this.props;
    return navigation.goBack();
  };
  render() {
    const { navigation, themeType } = this.props;
    const styles = themeType === 'light' ? lightStyle : darkStyle;
    return (
      <Container safeAreaView={true} selectedThemeType={themeType}>
          <View style={styles.innerTextView}>
        
     <BangHeader>
      </BangHeader>
           </View>
      
      
        <LinearGradient start={{x: 1.55, y: 0.85}} end={{x: 0.5, y: 2.0}} colors={['#3E434E', '#1A1C1F']} style={styles.Qrcode}>
        <Text style={styles.ScanQrNoSession}>You’ll need to login at least once , in order
to get your session QR code generated</Text>

      <Text style={styles.ScanQr}>Scan your QR</Text>
         

        </LinearGradient>
        <View style={styles.innerTextView}>
        
     
        </View>
        <View style={styles.buttonView}>
          <ButtonEmpty
            isArrowBtnTrue
            onPress={() => navigation.navigate('Login')}
            style={styles.shadowStyle}
            shadowRadius={15}
            buttonStyle={styles.getStartBtn}
            title={'Already a member?Log in'}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('Registration')}>
            <Text style={styles.bottomText}>Or signup now</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

function mapStateToProps({ user }: any) {
  return { themeType: user.themeType };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({ addSelectedTheme: SelectedTheme }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);
